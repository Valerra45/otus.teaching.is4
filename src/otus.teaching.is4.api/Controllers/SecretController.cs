﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace otus.teaching.is4.api.Controllers
{
    [ApiController]
    [Route("secret")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class SecretController : ControllerBase
    {
        public string Index()
        {
            return "Hi! Secret message from Api!";
        }
    }
}
